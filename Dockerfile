FROM index.alauda.cn/alaudaorg/alaudabase:ubuntu-14.04-global-1.1

RUN apt-get update && apt-get install -y pgbouncer && rm -rf /var/lib/apt/lists/*

RUN groupadd -r pgbouncer && useradd -r -g pgbouncer pgbouncer \
    && echo "daemon off;" >> /etc/nginx/nginx.conf \
    && rm -rf /etc/nginx/conf.d/default.conf

COPY requirements.txt .
RUN pip install --no-cache-dir -r requirements.txt

COPY requirements-alauda.txt /
RUN pip install --no-cache-dir --trusted-host pypi.alauda.io --extra-index-url http://mathildetech:Mathilde1861@pypi.alauda.io/simple/ -r /requirements-alauda.txt

COPY run.sh .
RUN chmod +x run.sh && mkdir /var/log/mathilde && chmod +w /var/log/mathilde

EXPOSE 8080 80
CMD ["/run.sh"]

WORKDIR /app
COPY . /app

RUN ln -s /app/conf/nginx.conf /etc/nginx/conf.d/nginx.conf \
    && ln -s /app/conf/supervisord.conf /etc/supervisord.conf
