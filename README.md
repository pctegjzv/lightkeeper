## What does it do
As the alternative of feature flag, it is more understandable and flexible.
It is a service that provides weblabs. A weblab can be considered as a switch
which controls whether a new feature can be seen by customers.

## How does it work
You can call APIs to create/update/view weblabs. The most important API is the
one which is used to retrieve the weblab's state.

This service actually includes two micro services. The one with port `8080` is for CRUD operations
on weblabs and whitelists. The other one with port `80` is for querying
weblab states.

## How use it in the code
Set ENV
```
LIGHTKEEPER_ENDPOINT=http://lightkeeper.int.alauda.cn/v1/ # For INT
LIGHTKEEPER_ENDPOINT=http://lightkeeper.alauda.cn/v1/ # For CN
```

Install the pypi package `sange` by following steps
```
curl https://bootstrap.pypa.io/get-pip.py | python

pip install --trusted-host pypi.alauda.io \
    --extra-index-url http://mathildetech:Mathilde1861@pypi.alauda.io/simple/ \
    sange
```

Then in your code, you can do
```
from alauda.weblab import WeblabClient


result = WeblabClient.prefetch(namespace=namespace, weblabs=[])

if result.get('PERMISSION_ENABLED'):
    # do something

# View weblab states
print 'weblab states: {}'.format(result.data)
```

Without calling remote weblab service, you can set ENV var on your service. It's convenient for debugging code. Note, do NOT set this ENV var in production/testing services! It's ONLY for debugging local code.

```
LIGHTKEEPER_OVERRIDDEN_WEBLABS=EVENT_ENABLED:1,PERMISSION_ENABLED:0
```

## Run it in docker
### EVN vars

- `DEBUG`: default value is `False`
- `DB_ENGINE`: default value is `django.db.backends.sqlite3`. The other option is `django.db.backends.postgresql_psycopg2`
- `DB_NAME`: default value is `/tmp.db`
- `DB_USER`:
- `DB_PASSWORD`:
- `DB_HOST`: default value is `127.0.0.1`
- `DB_PORT`: default is `6432`
- `SOURCE`: 1029

###Configure normal-model redis
- `REDIS_TYPE_WRITER`: normal
- `REDIS_HOST_WRITER`: the redis host for writing
- `REDIS_PORT_WRITER`: the redis port, e.g. 6379
- `REDIS_DB_NAME_WRITER`: 0
- `REDIS_TYPE_READER`: normal
- `REDIS_HOST_READER`: the redis host for reading
- `REDIS_PORT_READER`: the redis port, e.g. 6379
- `REDIS_DB_NAME_READER`: 0

###Configure cluster-model redis
- `REDIS_TYPE_WRITER`: cluster
- `REDIS_STARTUP_NODES_WRITER`: 127.0.0.1:7000,127.0.0.1:7001
- `REDIS_TYPE_READER`: cluster
- `REDIS_STARTUP_NODES_READER`: 127.0.0.1:7002,127.0.0.1:7003
- `REDIS_READONLY_MODE_READER`: true

### Command


```
$ docker run -d --name lightkeeper_dev -p 10080:8080 --env-file=lightkeeper.env lightkeeper

$ docker exec lightkeeper_dev python manage.py migrate
```

## APIs

### Create a new weblab
`POST /v1/weblabs`
```json
{
    "name": "AWS_CLOUDWATCH_METRIC_ENABLED",
    "description": "Show metrics from AWS cloud watch",
    "created_by": "chennanfei",
    "is_enabled": false
}
```

- `name`: [Required][String] the weblab name which should only contains A-Z, 0-9 and _. Note, it is unique.
- `description`: [Required][String] the weblab description which tells the purpose
- `created_by`: [Required][String] the person who creates the weblab
- `is_enabled`: [Required][Boolean] the weblab state


### List weblabs
`GET /v1/weblabs`


### Retrieve a weblab info
`GET /v1/weblabs/(weblab_name)`


### Update a weblab info
`PUT /v1/weblabs/(weblab_name)`
```json
{
    "description": "Show metrics/alarm from AWS cloud watch",
    "is_enabled": true
}
```

### Delete weblab
`DELETE /v1/weblabs/(weblab_name)`


### Create a weblab whitelist for a special namespace
`POST /v1/weblabs/(weblab_name)/whitelists`
```json
{
    "namespace": "chennanfei",
    "is_enabled": true
}
```


### List weblab whitelists
`GET /v1/weblabs/(weblab_name)/whitelists`


### Retrieve whitelist info
`GET /v1/weblabs/(weblab_name)/whitelists/(whitelist_id)`


### Update whitelist info
`PUT /v1/weblabs/(weblab_name)/whitelists/(whitelist_id)`

```
{
    "is_enabled": false
}
```


### Delete whitelist info
`DELETE /v1/weblabs/(weblab_name)/whitelists/(whitelist_id)`

### Note, the most APIs are here! A slash should be at the tail!
`GET /v1/weblab-states/`: Retrieve default states of all weblabs

`GET /v1/weblab-states/(namespace)/`: Retrieve all weblab states of the namespace 

Response:

```
{
	"weblab_name_1": true,
	"weblab_name_2": false
}
```

Optional params:
`weblabs`: e.g. 'weblab_name_1,weblab_name_2,weblab_name_3'


## Example
```python
import requests
import traceback


def get_weblab_states(namespace=None, weblabs=None):
    url = 'http://<lightkeeper_host>/v1/weblab-states/'
    if namespace:
        url += namespace + '/'

    if weblabs:
        url += '?weblabs=' + ','.join(weblabs)

    try:
        response = requests.get(url)
        if response.status_code == 200:
            return response.json()
    except:
        print traceback.format_exc()

    return {}

states = get_weblab_states('chennanfei', weblabs=['APP_ENABLED'])
if states.get('APP_ENABLED'):
    print 'Do new thing'
else:
    print 'Do old thing'
```
