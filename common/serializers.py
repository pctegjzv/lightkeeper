from rest_framework.pagination import PageNumberPagination
from rest_framework.response import Response


class ResultsPaginationSerializer(PageNumberPagination):

    def get_paginated_response(self, data):
        return Response({
            'count': self.page.paginator.count,
            'num_pages': self.page.paginator.num_pages,
            'page_size': self.page_size,
            'results': data
        })
