import time
from io_handlers.db_clients import check_database
from weblabs.cache import check_read_redis, check_write_redis


class DiagnoseClient(object):
    @staticmethod
    def check():
        result = {
            'status': 'OK',
            'details': []
        }
        callbacks = [
            check_database,
            check_read_redis,
            check_write_redis
        ]

        for callback in callbacks:
            start_time = time.time()
            check_result = callback()
            check_result['latency'] = '{}ms'.format((time.time() - start_time) * 1000)

            result['details'].append(check_result)
            if check_result['status'] != 'OK':
                result['status'] = 'ERROR'

        return result
