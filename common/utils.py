import pytz
import hmac
import hashlib
from datetime import timedelta
from datetime import datetime
from mathilde import settings


def create_signature(secret, text):
    return 'sha1=' + hmac.new(secret.encode('utf-8'), text,
                              hashlib.sha1).hexdigest()


def now_time():
    return pytz.timezone(settings.TIME_ZONE).localize(datetime.now())


def offset_time(dt, seconds):
    return dt + timedelta(seconds=seconds)


def str_to_bool(value):
    if not isinstance(value, bool):
        value = value in ['True', '1'] and True or False
    return value
