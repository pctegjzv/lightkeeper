from rest_framework import permissions, viewsets
from django.contrib.auth.models import AnonymousUser
from rest_framework.response import Response
from rest_framework.status import HTTP_200_OK


class AllowAny(permissions.BasePermission):
    def has_permission(self, request, view):
        return True


class AllowAnyModelViewSet(viewsets.ModelViewSet):
    permission_classes = (AllowAny,)

    def perform_authentication(self, request):
        return AnonymousUser()


class HealthCheckViewSet(AllowAnyModelViewSet):
    def ping(self, request):
        return Response(status=HTTP_200_OK)
