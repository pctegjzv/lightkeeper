from django.conf.urls import url
from django.conf.urls import include
from django.conf.urls import patterns
from mathilde.routers import ApiRouter
from mathilde import settings
from common.views import HealthCheckViewSet

router = ApiRouter()


urlpatterns = patterns(
    '',
    url(r'^', include(router.urls)),

    url(r'^ping/?$'.format(settings.APP_URL_REGEX),
        HealthCheckViewSet.as_view({'get': 'ping'}),
        name='ping'),

    # do NOT add slash at the tail
    url(r'^weblabs', include('weblabs.urls')),

    url(r'^weblab-cache', include('weblabs.urls_cache')),
)
