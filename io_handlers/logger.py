from datetime import datetime
from io_handlers import constants
import traceback


LOG_MODES = {
    'DEBUG': 0,
    'INFO': 1,
    'WARN': 2,
    'ERROR': 3
}


def _log(msg, level='DEBUG'):
    mode = LOG_MODES.get(constants.LOG_LEVEL, 0)
    if LOG_MODES.get(level) < mode:
        return

    now = datetime.now().strftime('%Y-%m-%d %H:%M:%S')
    print '{} [{}] {}'.format(now, level, msg)


def debug(msg):
    _log(msg)


def info(msg):
    _log(msg, level='INFO')


def warn(msg):
    _log(msg, level='WARN')


def error(msg):
    _log(msg, level='ERROR')
    print traceback.format_exc()
