from weblabs.cache import WeblabCache
from io_handlers import logger
from io_handlers.db_clients import get_db_client
from mathilde.settings import OVERRIDDEN_WEBLABS


SQL_NAMESPACE_STATES = ("SELECT name, is_enabled FROM weblabs_weblabwhitelist "
                        "WHERE namespace='%(namespace)s'")

SQL_ALL_WEBLAB_STATES = 'SELECT name, is_enabled FROM weblabs_weblab'


def close_db_conn(method):
    def wrapper(self, *args, **kwargs):
        result = method(self, *args, **kwargs)

        if self._db_client:
            self._db_client.close()
            logger.debug('DB connection was closed')

        return result

    return wrapper


class WeblabClient(object):
    def __init__(self):
        self._db_client = None

    def get_state(self, namespace, weblab_name):
        logger.debug('Retrieving weblab states for namespace ({}) and weblab ({})'
                     .format(namespace, weblab_name))

        states = self.get_states(namespace)
        return {weblab_name: states.get(weblab_name)}

    def _get_db_client(self):
        if self._db_client is None:
            self._db_client = get_db_client()
        return self._db_client

    @close_db_conn
    def get_states(self, namespace, weblab_names=None):
        if OVERRIDDEN_WEBLABS:
            return self._get_weblabs_from_env()

        weblab_states = self._get_default_states()

        if namespace:
            whitelisted_states = self._get_namespace_states(namespace)
            for k, v in weblab_states.items():
                if k in whitelisted_states:
                    weblab_states[k] = whitelisted_states[k]

        # if specific weblab names are passed, just return them
        if weblab_names:
            return {x: weblab_states.get(x) for x in weblab_names if x}

        logger.info('Namespace: {}, states: {}'.format(namespace, weblab_states))
        return weblab_states

    def _get_weblabs_from_env(self):
        # fix HTZQ-158. for private deployment, maybe no need run query from redis or database
        overridden_states = {}
        overridden_weblabs = OVERRIDDEN_WEBLABS.split(',')
        for weblab in overridden_weblabs:
            temp_list = weblab.split(':')
            if len(temp_list) != 2 or temp_list[1] not in ['0', '1']:
                logger.error('Overridden weblab (%s) is invalid!' % weblab)
                return {}

            overridden_states[temp_list[0]] = bool(temp_list[1] == '1')

        return overridden_states

    def _get_default_states(self):
        logger.debug('Retrieving default weblab states')

        weblab_states = WeblabCache.get_weblab_states()
        if not weblab_states:
            weblab_states = self._query_weblab_states(SQL_ALL_WEBLAB_STATES)
            WeblabCache.reset_cache(weblab_states)
            logger.debug('Weblab states cache was set')

        logger.debug('Default weblab states: {}'.format(weblab_states))
        return weblab_states

    def _get_namespace_states(self, namespace):
        logger.debug('Retrieving weblab states for namespace (%s)' % namespace)

        whitelisted_states = WeblabCache.get_weblab_states(namespace)
        if whitelisted_states is None:
            sql_stat = SQL_NAMESPACE_STATES % {'namespace': namespace}
            whitelisted_states = self._query_weblab_states(sql_stat)

            WeblabCache.reset_cache(whitelisted_states, namespace)
            logger.debug('Weblab states of namespace (%s) cache was set' % namespace)

        logger.debug('Whitelisted weblab states: {}'.format(whitelisted_states))
        return whitelisted_states

    def _query_weblab_states(self, sql_stat):
        results = self._get_db_client().execute_query(sql_stat)
        logger.debug('Weblab states: {}'.format(results))
        return results and {x[0]: bool(x[1]) for x in results} or {}
