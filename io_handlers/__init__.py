import tornado.web
import json

from common.diagnose import DiagnoseClient
from io_handlers.state_client import WeblabClient
from io_handlers import logger


class WebRequestHandler(tornado.web.RequestHandler):
    def prepare(self):
        self.content_type = self.request.headers.get('content-type')

    def _create_response(self, content='', status_code=200, content_type='application/json'):
        self.set_header('Content-Type', content_type)
        self.set_status(status_code)
        self.write(content)

    @property
    def post_data(self):
        if self.content_type != 'application/json':
            raise Exception('Wrong content type for POST request. It should be application/json')

        try:
            return json.loads(self.request.body)
        except:
            logger.error('Wrong data type. It should be json format')
            raise


class DiagnoseCheckHandler(WebRequestHandler):
    def get(self):
        result = DiagnoseClient.check()
        self._create_response(result)


class HealthCheckHandler(WebRequestHandler):
    def get(self):
        self._create_response('Lightkeeper: They say that was I who carried the first light into '
                              'the universe. They might be right, I cannot quite recall.')


class WeblabStatesHandler(WebRequestHandler):
    def get(self, namespace=None):
        logger.info('Request user agent: {}'.format(self.request.headers.get('user-agent')))
        logger.info('Request client ip: {}'.format(self.request.remote_ip))

        weblab_names = self.get_argument('weblabs', None)
        if weblab_names:
            weblab_names = weblab_names.split(',')

        states = WeblabClient().get_states(namespace, weblab_names)
        self._create_response(states)
