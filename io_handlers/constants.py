import os


def str_to_bool(text):
    return text and text.lower() in ['yes', 'true']


DEBUG = str_to_bool(os.getenv('DEBUG', 'false'))

DB_ENGINE = os.getenv('DB_ENGINE', 'sqlite3')
DB_HOST = os.getenv('DB_HOST')
DB_PORT = os.getenv('DB_PORT')
DB_NAME = os.getenv('DB_NAME', '/tmp.db')
DB_USER = os.getenv('DB_USER')
DB_PASSWORD = os.getenv('DB_PASSWORD')

LOG_LEVEL = os.getenv('LOG_LEVEL', 'DEBUG')
