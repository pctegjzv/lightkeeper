import psycopg2
import sqlite3
import MySQLdb

from io_handlers import logger
from io_handlers import constants


class DBClient(object):
    def execute_query(self, sql_stat):
        pass

    def close(self):
        if self.conn:
            self.conn.close()


class SqliteClient(DBClient):
    def __init__(self):
        self.conn = sqlite3.connect(constants.DB_NAME)

    def execute_query(self, sql_stat):
        logger.debug('Sqlite is executing query with sql %s' % sql_stat)

        try:
            cursor = self.conn.execute(sql_stat)
            results = []
            for row in cursor:
                results.append(row)

            return results
        except:
            logger.error('Failed to execute db query!')
            self.conn.close()
            raise


class PostgreClient(DBClient):
    def __init__(self):
        # use pgbouncer
        self.conn = psycopg2.connect(
            host=constants.DB_HOST,
            port=int(constants.DB_PORT),
            database=constants.DB_NAME,
            user=constants.DB_USER,
            password=constants.DB_PASSWORD
        )

    def execute_query(self, sql_stat):
        logger.debug('Postgre is executing query with sql %s' % sql_stat)

        cur = self.conn.cursor()
        try:
            cur.execute(sql_stat)
            results = []
            result = cur.fetchone()
            while result:
                results.append(result)
                result = cur.fetchone()

            return results
        except:
            logger.error('Failed to execute db query!')
            self.conn.close()
            raise
        finally:
            cur.close()


class MySQLClient(DBClient):
    def __init__(self):
        self.conn = MySQLdb.connect(
            host=constants.DB_HOST,
            port=int(constants.DB_PORT),
            db=constants.DB_NAME,
            user=constants.DB_USER,
            password=constants.DB_PASSWORD
        )

    def execute_query(self, sql_stat):
        logger.debug('MySQL is executing query with sql %s' % sql_stat)

        cur = self.conn.cursor()
        try:
            cur.execute(sql_stat)
            results = []
            result = cur.fetchone()
            while result:
                results.append(result)
                result = cur.fetchone()

            return results
        except:
            logger.error('Failed to execute db query!')
            self.conn.close()
            raise


def get_db_client():
    logger.debug('Creating DB connection')

    if constants.DB_ENGINE == 'postgresql':
        return PostgreClient()
    elif constants.DB_ENGINE == 'sqlite3':
        return SqliteClient()
    elif constants.DB_ENGINE == 'mysql':
        return MySQLClient()

    raise Exception('DB engine %s is not supported' % constants.DB_ENGINE)


def check_database():
    check_result = {
        'status': 'OK',
        'name': 'database',
        'message': None,
        'suggestion': None,
    }

    try:
        client = get_db_client()
        client.close()
    except Exception as ex:
        check_result['status'] = 'ERROR'
        check_result['message'] = '{} - {}'.format(type(ex), ex.message)
        check_result['suggestion'] = 'Check database {}:{}. Ensure they are correct'\
            .format(constants.DB_HOST, constants.DB_PORT)

    return check_result
