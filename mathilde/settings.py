"""
Django settings for mathilde project.

For more information on this file, see
https://docs.djangoproject.com/en/1.7/topics/settings/

For the full list of settings and their values, see
https://docs.djangoproject.com/en/1.7/ref/settings/
"""

# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
import os
BASE_DIR = os.path.dirname(os.path.dirname(__file__))


# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/1.7/howto/deployment/checklist/

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = 'yp*n$0^o8#nzd(mq0id_d6=low%gmo($3td!@*bv5mh8@fqt#c'

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = os.getenv('DEBUG', False)

TEMPLATE_DEBUG = os.getenv('DEBUG', False)

ALLOWED_HOSTS = ['*']

# Application definition
INSTALLED_APPS = (
    'django.contrib.sessions',
    'django.contrib.messages',
    'weblabs'
)

MIDDLEWARE_CLASSES = (
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
    'mekansm.contrib.django.middleware.MekAPIMiddleware',
)

ROOT_URLCONF = 'mathilde.urls'

REST_FRAMEWORK = {
    'EXCEPTION_HANDLER': 'mekansm.contrib.rest_framework.exception_handler.exception_handler',
    'DEFAULT_PAGINATION_CLASS': 'common.serializers.ResultsPaginationSerializer',
    'PAGE_SIZE': 50
}

WSGI_APPLICATION = 'mathilde.wsgi.application'

# Database
# https://docs.djangoproject.com/en/1.7/ref/settings/#databases
DATA_TYPE_MAP = {
    'mysql': 'django.db.backends.mysql',
    'postgresql': 'django.db.backends.postgresql_psycopg2',
    'sqlite3': 'django.db.backends.sqlite3'
}

DATABASES = {
    'default': {
        'ENGINE': DATA_TYPE_MAP.get(os.getenv('DB_ENGINE', 'sqlite3')),
        'NAME': os.getenv('DB_NAME', '/tmp.db'),
        'USER': os.getenv('DB_USER', ''),
        'PASSWORD': os.getenv('DB_PASSWORD', ''),
        'HOST': os.getenv('DB_HOST', ''),
        'PORT': os.getenv('DB_PORT', ''),
    }
}

# Internationalization
# https://docs.djangoproject.com/en/1.7/topics/i18n/

LANGUAGE_CODE = 'en-us'

TIME_ZONE = 'UTC'

USE_I18N = True

USE_L10N = True

USE_TZ = True

# logging
LOG_PATH = '/var/log/mathilde/lightkeeper'
LOG_LEVEL = os.getenv('LOG_LEVEL', 'DEBUG')
LOG_HANDLER = os.getenv('LOG_HANDLER', 'debug,info,error').split(',')

LOGGING = {
    'version': 1,
    'disable_existing_loggers': True,
    'formatters': {
        'standard': {
            'format': '%(asctime)s [%(levelname)s][%(threadName)s]' +
                      '[%(name)s:%(lineno)d] %(message)s'}
    },
    'handlers': {
        'debug': {
            'level': 'DEBUG',
            'class': 'logging.handlers.RotatingFileHandler',
            'filename': LOG_PATH + '.debug.log',
            'maxBytes': 1024 * 1024 * 100,
            'backupCount': 5,
            'formatter': 'standard',
        },
        'info': {
            'level': 'INFO',
            'class': 'logging.handlers.RotatingFileHandler',
            'filename': LOG_PATH + '.info.log',
            'maxBytes': 1024 * 1024 * 100,
            'backupCount': 5,
            'formatter': 'standard',
        },
        'error': {
            'level': 'ERROR',
            'class': 'logging.handlers.RotatingFileHandler',
            'filename': LOG_PATH + '.error.log',
            'maxBytes': 1024 * 1024 * 100,
            'backupCount': 5,
            'formatter': 'standard',
        },
        'console': {
            'level': LOG_LEVEL,
            'class': 'logging.StreamHandler',
            'formatter': 'standard'
        },
    },
    'loggers': {
        'django': {
            'handlers': LOG_HANDLER,
            'level': 'INFO',
            'propagate': False
        },
        'django.request': {
            'handlers': LOG_HANDLER,
            'level': 'INFO',
            'propagate': False,
        },
        '': {
            'handlers': LOG_HANDLER,
            'level': LOG_LEVEL,
            'propagate': False
        },
    }
}

# custom settings
APP_URL_REGEX = '[A-Za-z0-9-_.]+'

EMAIL = {
    'template_path': 'email',
    'from_mailbox': os.getenv('EMAIL_FROM') or 'info@mathildetech.com',
    'debug_cc_email': 'rancheng@mathildetech.com',
    'rubick_url': os.getenv('RUBICK_URL') or 'https://www.alauda.io'
}

JAKIRO_USER = {
    'username': os.getenv('JAKIRO_USERNAME', 'sys_admin'),
    'password': os.getenv('JAKIRO_PASSWORD', '07Apples'),
}

OVERRIDDEN_WEBLABS = os.getenv('OVERRIDDEN_WEBLABS', None)
