from mekansm.exceptions import MekAPIException


class WeblabException(MekAPIException):
    errors_map = {
        'inconsistent_cache_states': {
            'message': 'Weblab states in cache are consistent'
        }
    }
