import time

from redis.exceptions import ConnectionError

from common.utils import str_to_bool
from alauda_redis_client import RedisClientFactory


__all__ = ['CacheProtector', 'WeblabCache']


WEBLAB_ALL = 'ALAUDA:WEBLAB_ALL'
NAMEPSACE_PREFIX = 'ALAUDA:WEBLAB_NAMESPACE'
RESERVED_WEBLAB_KEY = 'ALAUDA:WEBLAB:RESERVED'

REDIS_EXPIRE = 7200

REDIS_SERVER = RedisClientFactory.get_client_by_key('WRITER')

REDIS_READ_SERVER = RedisClientFactory.get_client_by_key('READER')

MAX_CONN_ATTEMPTS = 3
MAX_CONN_LOCK_TIMEOUT = 60


def get_key(namespace=None):
    if namespace:
        return '%s:%s' % (NAMEPSACE_PREFIX, namespace)
    else:
        return WEBLAB_ALL


def handle_exception(func):
    def wrapper(*args, **kwargs):
        if not CacheProtector.can_connect():
            return None

        try:
            result = func(*args, **kwargs)
            CacheProtector.succeed()
            return result
        except Exception as ex:
            if isinstance(ex, ConnectionError):
                CacheProtector.fail()

            print 'Cache service failure happened. {} - {}'.format(type(ex), ex.message)
            return None

    return wrapper


class CacheProtector(object):
    attempts = 0
    locked_at = None

    @classmethod
    def can_connect(cls):
        if cls.locked_at is None:
            return True

        if (time.time() - cls.locked_at) >= MAX_CONN_LOCK_TIMEOUT:
            return True

        return False

    @classmethod
    def fail(cls):
        cls.attempts += 1
        if cls.attempts >= MAX_CONN_ATTEMPTS:
            cls.locked_at = time.time()

    @classmethod
    def succeed(cls):
        cls.attempts = 0
        cls.locked_at = None


class WeblabCache(object):

    @staticmethod
    @handle_exception
    def delete(weblab_name, namespace=None):
        key = get_key(namespace)
        REDIS_SERVER.hdel(key, weblab_name)

    @staticmethod
    @handle_exception
    def get_state(weblab_name):
        key = get_key()
        is_enabled = REDIS_READ_SERVER.hget(key, weblab_name)
        if is_enabled is not None:
            is_enabled = str_to_bool(is_enabled)
        return is_enabled

    @staticmethod
    @handle_exception
    def get_weblab_states(namespace=None):
        key = get_key(namespace)
        states_dict = REDIS_READ_SERVER.hgetall(key)
        if namespace and not states_dict:
            return None

        if RESERVED_WEBLAB_KEY in states_dict:
            del states_dict[RESERVED_WEBLAB_KEY]

        return {k: str_to_bool(v) for k, v in states_dict.items()}

    @staticmethod
    @handle_exception
    def reset_cache(state_dict=None, namespace=None):
        key = get_key(namespace)
        REDIS_SERVER.delete(key)

        # cache something if any DB query runs but no real data
        if not state_dict:
            state_dict = {RESERVED_WEBLAB_KEY: True}

        REDIS_SERVER.hmset(key, state_dict)
        REDIS_SERVER.expire(key, REDIS_EXPIRE)

    @staticmethod
    @handle_exception
    def clean_cache(namespace=None):
        key = get_key(namespace)
        REDIS_SERVER.delete(key)

    @staticmethod
    @handle_exception
    def states_exist(namespace=None):
        key = get_key(namespace)
        return REDIS_READ_SERVER.exists(key)


def _check_redis(name, server):
    check_result = {
        'status': 'OK',
        'name': name,
        'message': None,
        'suggestion': None,
    }

    try:
        server.ping()
    except Exception as ex:
        check_result['status'] = 'ERROR'
        check_result['message'] = '{} - {}'.format(type(ex), ex.message)
        check_result['suggestion'] = 'Ensure your redis is running!'

    return check_result


def check_read_redis():
    return _check_redis('redis_for_read', REDIS_READ_SERVER)


def check_write_redis():
    return _check_redis('redis_for_write', REDIS_SERVER)
