from django.conf.urls import url
from mathilde import settings
from weblabs.views import WeblabCacheView


REGEX = settings.APP_URL_REGEX

urlpatterns = [
    url(r'^/(?P<namespace>{})/?$'.format(REGEX),
        WeblabCacheView.as_view({'put': 'reset'}),
        name='cache_namespace')
]
