import logging

from datetime import datetime
from weblabs.cache import WeblabCache
from weblabs.exceptions import WeblabException
from weblabs.models import Weblab, WeblabWhitelist, WeblabRecord
from weblabs.serializers import WeblabSerializer, WeblabWhitelistSerializer, WeblabRecordSerializer
from mathilde.settings import OVERRIDDEN_WEBLABS

logger = logging.getLogger(__name__)


class WeblabBaseClient(object):
    update_fields = []

    def _get_updatable_data(self, data):
        update_data = {}
        for key in self.update_fields:
            if key in data:
                update_data[key] = data[key]

        return update_data


class WeblabClient(WeblabBaseClient):
    update_fields = ['description', 'is_enabled']

    def create_weblab(self, data):
        logger.debug('creating new weblab with data {}'.format(data))
        if OVERRIDDEN_WEBLABS:
            raise Exception('OVERRIDDEN_WEBLABS is set. Cannot create weblabs anymore.')

        data['is_enabled'] = False

        serializer = WeblabSerializer(data=data)
        serializer.is_valid(raise_exception=True)
        weblab = serializer.save()

        # save a record
        self._save_weblab_record(weblab.name, data={'created_by': weblab.created_by})

        return weblab, serializer.data

    @staticmethod
    def _retrieve_weblab(weblab_name):
        try:
            return Weblab.objects.get(name=weblab_name)
        except Exception as ex:
            logger.error('Failed to retrieve weblab {}. error: {} - {}'
                         .format(weblab_name, type(ex), ex.message))
            raise WeblabException('resource_not_exist')

    def _save_weblab_record(self, weblab_name, data={}):
        data['name'] = weblab_name
        results = WeblabRecord.objects.filter(name=weblab_name)
        if results:
            record_serializer = WeblabRecordSerializer(results[0], data=data, partial=True)
        else:
            record_serializer = WeblabRecordSerializer(data=data)

        record_serializer.is_valid(raise_exception=True)
        record_serializer.save()

    def get_weblab(self, name):
        logger.debug('retrieving weblab {}'.format(name))

        weblab = self._retrieve_weblab(name)
        serializer = WeblabSerializer(weblab)
        return weblab, serializer.data

    def get_weblabs(self, **params):
        logger.debug('retrieving weblabs with params {}'.format(params))

        if params:
            weblabs = Weblab.objects.filter(**params)
        else:
            weblabs = Weblab.objects.all()
        return weblabs, WeblabSerializer(weblabs, many=True).data

    def update_weblab(self, weblab_name, data):
        weblab = self._retrieve_weblab(weblab_name)
        serializer = WeblabSerializer(weblab,
                                      data=self._get_updatable_data(data),
                                      partial=True)
        serializer.is_valid(raise_exception=True)
        weblab = serializer.save()

        # save record
        if weblab.is_enabled:
            self._save_weblab_record(weblab_name, data={'opened_at': datetime.now(),
                                                        'created_by': weblab.created_by})

        return weblab, serializer.data

    def delete_weblab(self, weblab_name):
        weblab = self._retrieve_weblab(weblab_name)
        weblab.delete()

        # save record
        self._save_weblab_record(weblab_name, data={'removed_at': datetime.now(),
                                                    'created_by': weblab.created_by})

    def get_weblab_state(self, namespace, weblab_name):
        weblab_states = self.get_namespace_states(namespace)
        return {weblab_name: weblab_states.get(weblab_name)}

    def get_namespace_states(self, namespace):
        # query default states of all weblabs
        weblab_states = WeblabCache.get_weblab_states()
        if not weblab_states:
            weblabs = Weblab.objects.all()
            weblab_states = {x.name: x.is_enabled for x in weblabs}
            WeblabCache.reset_cache(weblab_states)
            logger.debug('Weblab states cache was set')

        # query whitelists of namespace
        whitelisted_states = WeblabCache.get_weblab_states(namespace)
        if whitelisted_states is None:
            whitelists = WeblabWhitelist.objects.filter(namespace=namespace)
            whitelisted_states = {x.weblab.name: x.is_enabled for x in whitelists}
            WeblabCache.reset_cache(whitelisted_states, namespace)
            logger.debug('Weblab states of namespace (%s) cache was set' % namespace)

        for k, v in weblab_states.items():
            if k in whitelisted_states:
                weblab_states[k] = whitelisted_states[k]

        return weblab_states


class WeblabWhitelistClient(WeblabBaseClient):
    update_fields = ['is_enabled']

    def create_whitelist(self, data):
        logger.debug('Creating new whitelist with data {}'.format(data))

        serializer = WeblabWhitelistSerializer(data=data)
        serializer.is_valid(raise_exception=True)
        whitelist = serializer.save()
        return whitelist, serializer.data

    @staticmethod
    def _retrieve_whitelist(whitelist_id):
        try:
            return WeblabWhitelist.objects.get(id=whitelist_id)
        except Exception as ex:
            logger.error('Failed to retrieve whitelist {}. error: {} - {}'
                         .format(whitelist_id, type(ex), ex.message))
            raise WeblabException('resource_not_exist')

    def get_whitelist(self, whitelist_id):
        logger.debug('Retrieving whitelist for {}'.format(whitelist_id))

        whitelist = self._retrieve_whitelist(whitelist_id)
        serializer = WeblabWhitelistSerializer(whitelist)
        return whitelist, serializer.data

    @staticmethod
    def get_whitelists(weblab_name, namespace=None):
        logger.debug('retrieving whitelists for {}'.format(weblab_name))

        params = {'weblab': weblab_name}
        if namespace:
            params['namespace'] = namespace

        return WeblabWhitelist.objects.filter(**params)

    def update_whitelist(self, whitelist_id, data):
        whitelist = self._retrieve_whitelist(whitelist_id)
        update_data = self._get_updatable_data(data)
        serializer = WeblabWhitelistSerializer(whitelist, update_data, partial=True)
        serializer.is_valid(raise_exception=True)
        whitelist = serializer.save()
        return whitelist, serializer.data

    def delete_whitelist(self, whitelist_id):
        whitelist = self._retrieve_whitelist(whitelist_id)
        whitelist.delete()
