from rest_framework.serializers import ModelSerializer, ValidationError

from weblabs.models import Weblab, WeblabWhitelist, WeblabRecord
import re


WEBLAN_NAME_REG = re.compile('^[A-Z][A-Z0-9_]+')


class WeblabSerializer(ModelSerializer):
    class Meta:
        model = Weblab
        read_only_fields = ('created_at', 'updated_at')

    def validate_name(self, value):
        if not WEBLAN_NAME_REG.match(value):
            raise ValidationError('Weblab name should only contain A-Z, 0-9 and _')
        return value


class WeblabWhitelistSerializer(ModelSerializer):
    class Meta:
        model = WeblabWhitelist


class WeblabRecordSerializer(ModelSerializer):
    class Meta:
        model = WeblabRecord
