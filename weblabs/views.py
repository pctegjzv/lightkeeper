from common.views import AllowAnyModelViewSet
from common.serializers import ResultsPaginationSerializer
from rest_framework.response import Response
from rest_framework import status
from weblabs.models import Weblab, WeblabWhitelist
from weblabs.serializers import WeblabSerializer, WeblabWhitelistSerializer
from weblabs.clients import WeblabClient, WeblabWhitelistClient
from weblabs.cache import WeblabCache
import logging

logger = logging.getLogger(__name__)


class WeblabView(AllowAnyModelViewSet):
    model = Weblab
    serializer_class = WeblabSerializer

    def create(self, request, *args, **kwargs):
        _, weblab_data = WeblabClient().create_weblab(request.data.copy())
        return Response(weblab_data, status=status.HTTP_201_CREATED)

    def retrieve(self, request, *args, **kwargs):
        weblab_name = kwargs['weblab_name']
        _, weblab_data = WeblabClient().get_weblab(weblab_name)
        return Response(weblab_data, status=status.HTTP_200_OK)

    def list(self, request, *args, **kwargs):
        params = {}
        for key in request.GET:
            params[key] = request.GET[key]
        _, weblabs_data = WeblabClient().get_weblabs(**params)
        return Response(weblabs_data, status=status.HTTP_200_OK)

    def update(self, request, *args, **kwargs):
        _, weblab_data = WeblabClient().update_weblab(kwargs['weblab_name'],
                                                      request.data)
        return Response(weblab_data, status=status.HTTP_200_OK)

    def destroy(self, request, *args, **kwargs):
        WeblabClient().delete_weblab(kwargs['weblab_name'])
        return Response(status=status.HTTP_204_NO_CONTENT)

    def retrieve_weblab_state(self, request, *args, **kwargs):
        data = WeblabClient().get_weblab_state(kwargs['namespace'], kwargs['weblab_name'])
        return Response(data, status=status.HTTP_200_OK)

    def list_namespace_states(self, request, *args, **kwargs):
        data = WeblabClient().get_namespace_states(kwargs['namespace'])
        return Response(data, status=status.HTTP_200_OK)


class WeblabWhitelistView(AllowAnyModelViewSet):
    model = WeblabWhitelist
    serializer_class = WeblabWhitelistSerializer

    def create(self, request, *args, **kwargs):
        data = request.data.copy()
        data.update({'weblab': kwargs['weblab_name']})
        _, whitelist_data = WeblabWhitelistClient().create_whitelist(data)
        return Response(whitelist_data, status.HTTP_201_CREATED)

    def retrieve(self, request, *args, **kwargs):
        _, whitelist_data = WeblabWhitelistClient().get_whitelist(
            kwargs['whitelist_id']
        )
        return Response(whitelist_data, status.HTTP_200_OK)

    def list(self, request, *args, **kwargs):
        whitelists = WeblabWhitelistClient().get_whitelists(kwargs['weblab_name'],
                                                            request.GET.get('namespace'))
        paginator = ResultsPaginationSerializer()
        page = paginator.paginate_queryset(whitelists, request)
        serializer = WeblabWhitelistSerializer(page, many=True)
        return paginator.get_paginated_response(serializer.data)

    def update(self, request, *args, **kwargs):
        _, whitelist_data = WeblabWhitelistClient().update_whitelist(
            kwargs['whitelist_id'],
            request.data
        )
        return Response(whitelist_data, status.HTTP_200_OK)

    def destroy(self, request, *args, **kwargs):
        WeblabWhitelistClient().delete_whitelist(kwargs['whitelist_id'])
        return Response(status=status.HTTP_204_NO_CONTENT)


class WeblabCacheView(AllowAnyModelViewSet):

    def reset(self, request, *args, **kwargs):
        namespace = kwargs['namespace']
        WeblabCache.clean_cache(namespace)
        data = WeblabClient().get_namespace_states(namespace)
        return Response(data, status=status.HTTP_200_OK)
