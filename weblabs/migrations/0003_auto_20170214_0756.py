# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import common.fields


class Migration(migrations.Migration):

    dependencies = [
        ('weblabs', '0002_weblabrecord'),
    ]

    operations = [
        migrations.AlterField(
            model_name='weblabwhitelist',
            name='id',
            field=common.fields.UuidField(auto_created=True,
                                          primary_key=True,
                                          serialize=False,
                                          editable=False,
                                          max_length=36,
                                          unique=True),
        ),
    ]
