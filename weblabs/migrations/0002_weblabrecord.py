from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('weblabs', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='WeblabRecord',
            fields=[
                ('name', models.CharField(max_length=60, serialize=False, primary_key=True)),
                ('created_by', models.CharField(max_length=30)),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('opened_at', models.DateTimeField(null=True, blank=True)),
                ('removed_at', models.DateTimeField(null=True, blank=True)),
            ],
        ),
    ]
