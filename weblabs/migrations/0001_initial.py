# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.db import models, migrations
import common.fields


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Weblab',
            fields=[
                ('name', models.CharField(max_length=60, serialize=False,
                                          primary_key=True)),
                ('description', models.TextField()),
                ('created_by', models.CharField(max_length=30)),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('updated_at', models.DateTimeField(auto_now=True)),
                ('is_enabled', models.BooleanField(default=False)),
            ],
            options={
                'ordering': ('name',),
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='WeblabWhitelist',
            fields=[
                ('id', common.fields.UuidField(auto_created=True,
                                               primary_key=True,
                                               serialize=False,
                                               editable=False,
                                               max_length=32,
                                               unique=True)),
                ('namespace', models.CharField(max_length=60)),
                ('is_enabled', models.BooleanField(default=True)),
                ('weblab', models.ForeignKey(to='weblabs.Weblab',
                                             db_column=b'name')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.AlterUniqueTogether(
            name='weblabwhitelist',
            unique_together=set([('namespace', 'weblab')]),
        ),
    ]
