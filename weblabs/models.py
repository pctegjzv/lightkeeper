from django.db import models
from django.db.models.signals import post_save, post_delete
from django.dispatch.dispatcher import receiver

from common.fields import UuidField
from weblabs.cache import WeblabCache


class Weblab(models.Model):
    name = models.CharField(max_length=60, primary_key=True)
    description = models.TextField()
    created_by = models.CharField(max_length=30)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    is_enabled = models.BooleanField(default=False)

    class Meta:
        ordering = ('name',)


class WeblabWhitelist(models.Model):
    id = UuidField(max_length=36, primary_key=True)
    namespace = models.CharField(max_length=60)
    weblab = models.ForeignKey(Weblab, db_column='name',
                               on_delete=models.CASCADE)
    is_enabled = models.BooleanField(default=True)

    class Meta:
        unique_together = ('namespace', 'weblab')


class WeblabRecord(models.Model):
    name = models.CharField(max_length=60, primary_key=True)
    created_by = models.CharField(max_length=30)
    created_at = models.DateTimeField(auto_now_add=True)
    opened_at = models.DateTimeField(blank=True, null=True)
    removed_at = models.DateTimeField(blank=True, null=True)


@receiver(post_save, sender=Weblab)
def post_save_weblab(sender, instance, created, **kwargs):
    weblabs = Weblab.objects.all()
    state_dict = {x.name: x.is_enabled for x in weblabs}
    WeblabCache.reset_cache(state_dict)


@receiver(post_delete, sender=Weblab)
def post_delete_weblab(sender, instance, **kwargs):
    WeblabCache.delete(instance.name)


@receiver(post_save, sender=WeblabWhitelist)
def post_save_whitelist(sender, instance, created, **kwargs):
    whitelists = WeblabWhitelist.objects.filter(namespace=instance.namespace)
    state_dict = {x.weblab.name: x.is_enabled for x in whitelists}
    WeblabCache.reset_cache(state_dict, namespace=instance.namespace)


@receiver(post_delete, sender=WeblabWhitelist)
def post_delete_whitelist(sender, instance, **kwargs):
    WeblabCache.delete(instance.weblab.name, instance.namespace)
