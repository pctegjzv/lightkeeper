from django.conf.urls import url
from mathilde import settings
from weblabs.views import WeblabView


REGEX = settings.APP_URL_REGEX

urlpatterns = [
    url(r'^/(?P<namespace>{})/(?P<weblab_name>{})/?$'.format(REGEX, REGEX),
        WeblabView.as_view({'get': 'retrieve_weblab_state'}),
        name='weblab_state'),

    url(r'^/(?P<namespace>{})/?$'.format(REGEX),
        WeblabView.as_view({'get': 'list_namespace_states'}),
        name='namespace_states')
]
