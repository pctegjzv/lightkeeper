from django.conf.urls import url
from mathilde import settings
from weblabs.views import WeblabView, WeblabWhitelistView


REGEX = settings.APP_URL_REGEX

urlpatterns = [
    url(r'^/?$', WeblabView.as_view({'get': 'list', 'post': 'create'}), name='weblabs'),

    url(r'^/(?P<weblab_name>{})/?$'.format(REGEX),
        WeblabView.as_view({'get': 'retrieve', 'put': 'update', 'delete': 'destroy'}),
        name='weblab'),

    url(r'^/(?P<weblab_name>{})/whitelists/?$'.format(REGEX),
        WeblabWhitelistView.as_view({'get': 'list', 'post': 'create'}),
        name='weblab_whitelists'),

    url(r'^/(?P<weblab_name>{})/whitelists/(?P<whitelist_id>{})/?$'.format(REGEX, REGEX),
        WeblabWhitelistView.as_view({'get': 'retrieve', 'put': 'update', 'delete': 'destroy'}),
        name='weblab_whitelist'),
]
