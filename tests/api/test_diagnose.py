from tests.api import BaseTestCase

import requests


class DiagnoseTestCase(BaseTestCase):
    state_endpoint = 'http://localhost:80/'

    def setUp(self):
        pass

    def test_ping(self):
        response = requests.get(self.state_endpoint + '_ping')
        self.assertEqual(response.status_code, 200)

    def test_diagnose(self):
        response = requests.get(self.state_endpoint + '_diagnose')
        self.assertEqual(response.status_code, 200)

        result = response.json()
        self.assertEqual(result['status'], 'OK')

        for detail in result['details']:
            self.assertEqual(detail['status'], 'OK')
