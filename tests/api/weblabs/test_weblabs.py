from django.core.urlresolvers import reverse
from rest_framework import status

from weblabs.cache import WeblabCache, CacheProtector
from tests.api import BaseTestCase
from weblabs.models import Weblab, WeblabWhitelist

WEBLAB_NAME = 'WEBLAB_API_TEST_ENABLED'
NAMESPACE = 'chennanfei-test'


class WeblabTestCaseBase(BaseTestCase):
    def setUp(self):
        # create weblab
        data = {
            'name': WEBLAB_NAME,
            'description': 'New feature env file',
            'created_by': NAMESPACE,
            'is_enabled': False
        }
        weblab = Weblab.objects.create(**data)

        # create whitelist
        nm_data = {
            'weblab': weblab,
            'namespace': NAMESPACE,
            'is_enabled': True
        }
        WeblabWhitelist.objects.create(**nm_data)

        super(WeblabTestCaseBase, self).setUp()

    def tearDown(self):
        Weblab.objects.all().delete()
        WeblabCache.clean_cache()
        WeblabCache.clean_cache(NAMESPACE)

        super(WeblabTestCaseBase, self).tearDown()

    def _create_weblab(self, weblab_name):
        data = {
            'name': weblab_name,
            'description': 'New feature env file',
            'created_by': NAMESPACE,
            'is_enabled': False
        }
        return Weblab.objects.create(**data)

    def _create_weblab_via_api(self, weblab_name):
        url = reverse('weblabs')
        response = self.client.post(url, data={
            'name': weblab_name,
            'is_enabled': False,
            'description': 'Test',
            'created_by': NAMESPACE
        })
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

    def _delete_weblab(self, weblab_name):
        Weblab.objects.get(name=weblab_name).delete()

    def _delete_weblab_via_api(self, weblab_name):
        url = reverse('weblab', kwargs={'weblab_name': weblab_name})
        response = self.client.delete(url)
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)

    def _create_whitelist(self, weblab_name, namespace):
        url = reverse('weblab_whitelists', kwargs={'weblab_name': weblab_name})
        response = self.client.post(url, data={
            'is_enabled': True,
            'namespace': namespace
        })
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)


class WeblabTestCase(WeblabTestCaseBase):

    def test_create_weblab(self):
        url = reverse('weblabs')
        data = {
            'name': 'WEBLAB_API_TEST_ENABLED_1',
            'description': 'New feature env file',
            'created_by': 'chennanfei',
            'is_enabled': True
        }
        response = self.client.post(url, data=data, format='json')
        result = self.parse_response(response)

        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertIsNotNone(result['created_at'])

    def test_update_weblab(self):
        url = reverse('weblab', kwargs={'weblab_name': WEBLAB_NAME})

        for is_enabled in [True, False]:
            data = {'is_enabled': is_enabled}
            response = self.client.put(url, data=data, format='json')
            result = self.parse_response(response)

            self.assertEqual(response.status_code, status.HTTP_200_OK)
            self.assertTrue(result['is_enabled'] == data['is_enabled'])
            self.assertTrue(result['updated_at'] > result['created_at'])

    def test_list_weblabs(self):
        url = reverse('weblabs')
        response = self.client.get(url)
        result = self.parse_response(response)

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertTrue(len(result))
        self.assertEqual(result[0]['name'], WEBLAB_NAME)

    def test_delete_weblab(self):
        weblab_name = 'WEBLAB_API_TEST_ENABLED_2'
        self._create_weblab(weblab_name)

        url = reverse('weblab', kwargs={'weblab_name': weblab_name})
        response = self.client.delete(url)

        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)

    def test_retrieve_weblab(self):
        url = reverse('weblab', kwargs={'weblab_name': WEBLAB_NAME})
        response = self.client.get(url)
        result = self.parse_response(response)

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertTrue(result['name'] == WEBLAB_NAME)

    # Depprecated
    def _retrieve_weblab_state(self):
        for namespace in [NAMESPACE, 'mathildedev']:
            WeblabCache.clean_cache(namespace)

            url = reverse('weblab_state', kwargs={
                'namespace': namespace,
                'weblab_name': WEBLAB_NAME
            })
            response = self.client.get(url)
            result = self.parse_response(response)

            self.assertEqual(response.status_code, status.HTTP_200_OK)

            # when namespace equals to NAMESPACE, the state is True
            self.assertEqual(result[WEBLAB_NAME], namespace == NAMESPACE)

    # Depprecated
    def _retrieve_namespace_states(self):
        for namespace in [NAMESPACE, 'mathildedev']:
            WeblabCache.clean_cache(namespace)

            url = reverse('namespace_states', kwargs={'namespace': namespace})
            response = self.client.get(url)
            result = self.parse_response(response)

            self.assertEqual(response.status_code, status.HTTP_200_OK)
            self.assertEqual(result[WEBLAB_NAME], namespace == NAMESPACE)

        state = WeblabCache.get_state(WEBLAB_NAME)
        self.assertIsNotNone(state)

    # Deprecated
    def _retrieve_nonexist_weblab_state(self):
        temp_weblab_name = 'WEBLAB_API_TEST_ENABLED_5'
        url = reverse('weblab_state', kwargs={'namespace': NAMESPACE,
                                              'weblab_name': temp_weblab_name})
        response = self.client.get(url)
        result = self.parse_response(response)

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertIsNone(result[temp_weblab_name])

    def test_cache_states_after_weblabs_created(self):
        temp_weblab_name = 'WEBLAB_API_TEST_ENABLED_3'
        self._create_weblab_via_api(temp_weblab_name)

        state = WeblabCache.get_state(temp_weblab_name)
        if CacheProtector.can_connect():
            self.assertIsNotNone(state)
        else:
            self.assertIsNone(state)

    def test_cache_states_after_weblabs_deleted(self):
        temp_weblab_name = 'WEBLAB_API_TEST_ENABLED_4'
        self._create_weblab_via_api(temp_weblab_name)
        self._delete_weblab(temp_weblab_name)

        state = WeblabCache.get_state(temp_weblab_name)
        self.assertIsNone(state)

    def test_retrieve_nonexist_weblab(self):
        temp_weblab_name = 'WEBLAB_API_TEST_ENABLED_5'

        # Retrieve weblab which doesn't exist
        url = reverse('weblab', kwargs={'weblab_name': temp_weblab_name})
        response = self.client.get(url)

        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

    def test_destroy_namespace_cache(self):
        weblab_name = 'TEMP_CACHE_TEST_NAME'
        namespace = 'temp_namespace'
        WeblabCache.reset_cache({weblab_name: True}, namespace)

        url = reverse('cache_namespace', kwargs={'namespace': namespace})
        response = self.client.put(url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

        result = self.parse_response(response)
        self.assertFalse(weblab_name in result)

    def test_retrieve_whitelists(self):
        url = reverse('weblab_whitelists', kwargs={'weblab_name': WEBLAB_NAME})
        response = self.client.get(url, data={'page': 1})
        self.assertEqual(response.status_code, status.HTTP_200_OK)

        result = self.parse_response(response)
        self.assertTrue(result['num_pages'] == 1)
        self.assertTrue(len(result['results']) > 0)

    def test_retrieve_whitelists_by_invalid_page(self):
        url = reverse('weblab_whitelists', kwargs={'weblab_name': WEBLAB_NAME})
        response = self.client.get(url, data={'page': 2})

        # page 2 does not exist
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

    def test_retrieve_whitelists_by_namespace(self):
        self._create_whitelist(WEBLAB_NAME, 'temp_test_user')

        url = reverse('weblab_whitelists', kwargs={'weblab_name': WEBLAB_NAME})
        response = self.client.get(url, data={'namespace': NAMESPACE})
        self.assertEqual(response.status_code, status.HTTP_200_OK)

        result = self.parse_response(response)
        whitelists = result['results']
        self.assertTrue(len(whitelists) > 0)

        for whitelist in whitelists:
            self.assertEqual(whitelist['namespace'], NAMESPACE)
