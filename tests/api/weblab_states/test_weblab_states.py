from weblabs.cache import WeblabCache, CacheProtector
from tests.api import BaseTestCase

import requests

WEBLAB_NAME = 'WEBLAB_API_TEST_ENABLED'
NAMESPACE = 'chennanfeitest'


class WeblabStateTestCase(BaseTestCase):
    weblab_endpoint = 'http://localhost:8080/v1/weblabs/'
    state_endpoint = 'http://localhost:80/v1/weblab-states/'

    def setUp(self):
        # create weblab
        data = {
            'name': WEBLAB_NAME,
            'description': 'New feature env file',
            'created_by': NAMESPACE,
            'is_enabled': False
        }
        requests.post(self.weblab_endpoint, data=data)

        # create whitelist
        nm_data = {
            'weblab': WEBLAB_NAME,
            'namespace': NAMESPACE,
            'is_enabled': True
        }
        url = self.weblab_endpoint + '{}/whitelists'.format(WEBLAB_NAME)
        requests.post(url, data=nm_data)

    def tearDown(self):
        url = '{}{}/'.format(self.weblab_endpoint, WEBLAB_NAME)
        requests.delete(url)

        WeblabCache.clean_cache()
        WeblabCache.clean_cache(NAMESPACE)

    def test_retrieve_weblab_state(self):
        for namespace in [NAMESPACE, 'mathildedev']:
            WeblabCache.clean_cache(namespace)

            url = self.state_endpoint + '{}/?weblabs={}'.format(namespace, WEBLAB_NAME)
            response = requests.get(url)
            self.assertEqual(response.status_code, 200)

            # when namespace equals to NAMESPACE, the state is True
            result = response.json()
            self.assertEqual(result[WEBLAB_NAME], namespace == NAMESPACE)

    def test_retrieve_weblab_states(self):
        response = requests.get(self.state_endpoint)
        self.assertEqual(response.status_code, 200)

        url = self.state_endpoint + '?weblabs={},{}'.format(WEBLAB_NAME, WEBLAB_NAME)
        response = requests.get(url)
        self.assertEqual(response.status_code, 200)

    def test_retrieve_namespace_states(self):
        for namespace in [NAMESPACE, 'mathildedev']:
            WeblabCache.clean_cache(namespace)

            url = '{}{}/'.format(self.state_endpoint, namespace)
            response = requests.get(url)
            self.assertEqual(response.status_code, 200)

            result = response.json()
            self.assertEqual(result[WEBLAB_NAME], namespace == NAMESPACE)

        state = WeblabCache.get_state(WEBLAB_NAME)
        if CacheProtector.can_connect():
            self.assertIsNotNone(state)
        else:
            self.assertIsNone(state)

    def test_retrieve_nonexist_weblab_state(self):
        temp_weblab_name = 'TEST_NONEXIST_WEBLAB_NAME'
        url = '{}?weblabs={}'.format(self.state_endpoint, temp_weblab_name)
        response = requests.get(url)
        self.assertEqual(response.status_code, 200)

        result = response.json()
        self.assertIsNone(result[temp_weblab_name])
