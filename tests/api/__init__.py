from rest_framework.test import APITestCase, APIClient
from django.http.response import HttpResponse
from rest_framework.response import Response

import json


class BaseTestCase(APITestCase):
    def setUp(self):
        self.client = APIClient()

    def tearDown(self):
        pass

    @staticmethod
    def parse_response(response):
        if response.status_code == 204:
            return None

        if isinstance(response, HttpResponse):
            return json.loads(response.content)
        elif isinstance(response, Response):
            return dict(response.data)

        raise Exception('Unsupported response type %s' % type(response))
