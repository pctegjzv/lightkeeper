import requests
import time
import os
import redis
from threading import Thread
from datetime import datetime


#ENDPOINT = 'http://internal-alauda-mflyshd-1252223894.us-west-2.elb.amazonaws.com/v1'
#ENDPOINT = 'http://internal-alauda-mflyshd-1252223894.us-west-2.elb.amazonaws.com:8080/v1'
ENDPOINT = 'http://localhost:80/v1'
MAX_WEBLABS = 100


class WeblabLatecyTest(object):
    t_max_times = 1000
    t_max_threads = 1

    def run_redis_test(self):
        host = 'int-registry.fjitx1.0001.usw2.cache.amazonaws.com'
        port = 6379

        redis_server = redis.Redis(host, port)
        test_key = 'ALAUDA:WEBLAB_TEST_KEY'
        data = {
            'weblab1': True,
            'weblab2': True,
            'weblab3': True,
            'weblab4': True,
            'weblab5': True
        }

        count, total = 0, 0
        while count < self.t_max_times:
            count += 1
            start_time = datetime.now()

            redis_server.hmset(test_key, data)
            redis_server.hget(test_key, 'weblab1')
            redis_server.delete(test_key)

            diff = (datetime.now() - start_time).total_seconds()
            total += diff
            print 'diff: {}, total: {}'.format(diff, total)

    def run_api_test(self):
        #path = '/ping/'
        path = '/weblab-states/vipertest/'
        print 'Testing path: %s' % path
        print 'Endpoint: %s' % ENDPOINT
        time.sleep(3)

        #self._clean_test_data()
        #self._prepare_test_data()
        self._test_path(path)

    def _prepare_test_data(self):
        print 'preparing test data'
        headers = {'content-type': 'application/json'}
        url = '{}/weblabs'.format(ENDPOINT)

        for index in xrange(MAX_WEBLABS):
            data = {
                "name": "SERVICE_METRIC_ENABLED_%d" % index,
                "description": "New feature env file",
                "created_by": "chennanfei",
                "is_enabled": False
            }
            requests.post(url, data=data, headers=headers)

    def _clean_test_data(self):
        print 'cleaning up test data'
        for index in xrange(MAX_WEBLABS):
            weblab_name = "SERVICE_METRIC_ENABLED_%d" % index
            url = '{}/weblabs/{}'.format(ENDPOINT, weblab_name)
            requests.delete(url)

    def _test_path(self, path):
        def callback(url, index):
            print 'thread index %d stared \n' % index

            count = 0
            t_max, t_min, t_sum = None, None, 0
            while count < self.t_max_times:
                time.sleep(1)
                count += 1
                start_time = datetime.now()
                response = requests.get(url)
                diff = (datetime.now() - start_time).total_seconds()

                t_sum += diff
                if t_max is None or t_max < diff:
                    t_max = diff

                if t_min is None or t_min > diff:
                    t_min = diff

                print (('thread: {}, count: {}, diff: {}, t_max: {}, t_min: {}, t_sum: {}, '
                       'status: {}').format(index, count, diff, t_max, t_min, t_sum,
                                            response.status_code))

            print 'thread index %d stopped \n' % index

        url = '%s%s' % (ENDPOINT, path)
        thread_index = 0

        while thread_index < self.t_max_threads:
            thread_index += 1

            Thread(target=callback, args=(url, thread_index)).start()


WeblabLatecyTest().run_api_test()
