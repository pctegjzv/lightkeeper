#!/bin/sh

cp /app/conf/pgbouncer.ini /tmp/pgbouncer.ini
sed -i "s/{{dbport}}/$DB_PORT/g ;s/{{dbname}}/$DB_NAME/g ;s/{{dbhost}}/$DB_HOST/g" /tmp/pgbouncer.ini
echo "\"$DB_USER\" \"$DB_PASSWORD\"" > /tmp/userlist.txt
rm /tmp/pgbouncer.pid

# Run supervisord in the foreground
/usr/local/bin/supervisord --nodaemon
