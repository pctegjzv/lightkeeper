from tornado import web, ioloop
from io_handlers import DiagnoseCheckHandler, HealthCheckHandler, WeblabStatesHandler


_url_handlers = [
    ('_ping/?', HealthCheckHandler),
    ('_diagnose/?', DiagnoseCheckHandler),
    ('v1/ping/?', HealthCheckHandler),
    ('v1/weblab-states/([a-z0-9]+)/?', WeblabStatesHandler),
    ('v1/weblab-states/?', WeblabStatesHandler)
]


def _generate_handlers():
    return [(r'/' + x[0], x[1]) for x in _url_handlers]


application = web.Application(
    handlers=_generate_handlers(),
    debug=True
)

if __name__ == '__main__':
    application.listen(80)

    try:
        ioloop.IOLoop.instance().start()
    except KeyboardInterrupt as ex:
        print 'Terminated by keyboard'
