import sys
import requests
import json
import os


CRUD_ENDPOINT = os.getenv('CRUD_ENDPOINT')
STATE_ENDPOINT = os.getenv('STATE_ENDPOINT')

OBJECTS = ['help', 'weblab', 'whitelist', 'state', 'cache']

ARGS_DICT = {
    '-o': {
        'alias': 'operation',
        'choices': ['create', 'update', 'delete', 'retrieve', 'reset']
    },
    '--creator': {
        'alias': 'created_by'
    },
    '--desc': {
        'alias': 'description'
    },
    '--name': {
        'alias': 'name'
    },
    '--namespace': {
        'alias': 'namespace'
    },
    '--state': {
        'alias': 'is_enabled',
        'choices': ['0', '1']
    },
}


def log(msg):
    print '> {}'.format(msg)


def parse_params():
    index = 0
    params = {'object': None}

    for arg in sys.argv:
        if index == 0:
            index = 1
            continue

        if index == 1:
            index += 1

            if arg in OBJECTS:
                params['object'] = arg
                continue

            raise Exception('The first arg should be one of {}'.format(OBJECTS))

        if arg in ARGS_DICT:
            params[ARGS_DICT[arg]['alias']] = sys.argv[index + 1]

        index += 1

    return params


def handle(response):
    try:
        log('Service result:')
        print '{}'.format(json.dumps(response.json(), indent=2))
    except:
        log('Response: {}'.format(response.content))

    if response.status_code < 200 or response.status_code > 299:
        raise Exception('Failed to request weblab service. Check the command parameters.')
    else:
        log('Service response is OK')


def help(operation, data):
    print """weblab.sh [help | weblab | whitelist | state | cache] [options]

    help: see command and options

    weblab:
        -o: [create|update|delete|retrieve]
        --creator: who creates the weblab
        --desc: describe the weblab
        --name: weblab name

        e.g.
            create a new weblab: weblab.sh weblab -o create --name xxx --desc xxx --creator xxx
            update a default state of ewblab: weblab.sh weblab -o update --name xxx --state 1

    whitelist
        -o: [create|update|delete|retrieve]
        --name: weblab name
        --namespace: namespace of which the weblab states cache
        --state: [0|1]. weblab state

        e.g
            open weblab for namespace: weblab.sh whitelist -o create --name xxx --namespace xxx --state 1

    state
        --name: weblab name. Optional
        --namespace: namespace of which the weblab states. Optional

        e.g
            retrieve default states of all weblabs: weblab.sh state
            retrieve weblab states of namespace: weblab.sh state --namespace xxx

    cache
        -o: [reset]. Reset weblab states cache for namespace
        --namespace: namespace of which the weblab states cache
"""


######################## Weblab ########################
def weblab(operation, data):
    is_enabled = data.get('is_enabled') == '1' and True or False

    if operation is None:
        operation = 'retrieve'

    if operation == 'retrieve':
        _retrieve_weblab(data.get('name'))
    elif operation == 'create':
        _create_weblab({
            'name': data.get('name'),
            'is_enabled': is_enabled,
            'description': data.get('description', ''),
            'created_by': data.get('created_by')
        })
    elif operation == 'delete':
        _delete_weblab(data.get('name'))
    elif operation == 'update':
        req_data = {'is_enabled': is_enabled}
        if 'description' in data:
            req_data['description'] = data['description']

        _update_weblab(data.get('name'), req_data)
    else:
        raise Exception('Operation %s is not supported' % operation)


def _retrieve_weblab(name):
    if name:
        log('Retrieving weblab %s' % name)
        url = '{}/weblabs/{}'.format(CRUD_ENDPOINT, name)
    else:
        log('Retrieving all weblabs')
        url = '{}/weblabs/'.format(CRUD_ENDPOINT)

    response = requests.get(url)
    handle(response)


def _create_weblab(data):
    log('Creating weblab')

    url = '{}/weblabs/'.format(CRUD_ENDPOINT)
    response = requests.post(url, data=data)
    handle(response)


def _delete_weblab(name):
    if not name:
        raise Exception('Weblab name is empty')

    log('Removing weblab %s' % name)

    url = '{}/weblabs/{}/'.format(CRUD_ENDPOINT, name)
    response = requests.delete(url)
    handle(response)


def _update_weblab(name, data):
    if not name:
        raise Exception('Weblab name is empty')

    log('Updating weblab %s' % name)

    url = '{}/weblabs/{}/'.format(CRUD_ENDPOINT, name)
    response = requests.put(url, data=data)
    handle(response)


######################## Weblab whitelist ########################
def whitelist(operation, data):
    is_enabled = data.get('is_enabled') == '1' and True or False
    name, namespace = data.get('name'), data.get('namespace')

    if operation is None:
        operation = 'retrieve'

    if operation == 'create':
        _create_whitelist(name, namespace, is_enabled)
    elif operation == 'retrieve':
        _retrieve_whitelist(name, namespace)
    elif operation == 'delete':
        _delete_whitelist(name, namespace)
    elif operation == 'update':
        _update_whitelist(name, namespace, is_enabled)
    else:
        raise Exception('Operation %s is not supported' % operation)


def _create_whitelist(name, namespace, is_enabled):
    if not name:
        raise Exception('Weblab name is empty')

    if not namespace:
        raise Exception('namespace is empty')

    log('Creating whitelist')

    url = '{}/weblabs/{}/whitelists/'.format(CRUD_ENDPOINT, name)
    response = requests.post(url, data={
        'namespace': namespace,
        'is_enabled': is_enabled
    })
    handle(response)


def _delete_whitelist(name, namespace):
    if not name:
        raise Exception('Weblab name is empty')

    if not namespace:
        raise Exception('namespace is empty')

    lid = _get_whitelist_id(name, namespace)
    log('Deleting whitelist %s' % lid)

    url = '{}/weblabs/{}/whitelists/{}/'.format(CRUD_ENDPOINT, name, lid)
    response = requests.delete(url)
    handle(response)


def _retrieve_whitelist(name, namespace):
    if not name:
        raise Exception('Weblab name is empty')

    log('Retrieving whitelists')

    url = '{}/weblabs/{}/whitelists/'.format(CRUD_ENDPOINT, name)
    if namespace:
        url += '?namespace=%s' % namespace

    response = requests.get(url)
    handle(response)

    return response


def _get_whitelist_id(name, namespace):
    response = _retrieve_whitelist(name, namespace)
    result = response.json()['results']
    if not result:
        raise Exception('Whitelist for ({}, {}) does not exist'.format(name, namespace))

    return result[0]['id']


def _update_whitelist(name, namespace, is_enabled):
    if not name:
        raise Exception('Weblab name is empty')

    if not namespace:
        raise Exception('namespace is empty')

    lid = _get_whitelist_id(name, namespace)
    log('Updating whitelist %s' % lid)

    url = '{}/weblabs/{}/whitelists/{}/'.format(CRUD_ENDPOINT, name, lid)
    response = requests.put(url, data={'is_enabled': is_enabled})
    handle(response)


######################## Weblab state ########################
def state(operation, data):
    log('Retrieving weblab states')

    name = data.get('name')
    namespace = data.get('namespace')

    url = '{}/weblab-states/'.format(STATE_ENDPOINT)
    if namespace:
        url += namespace + '/'

    if name:
        url += '?weblabs=%s' % name

    response = requests.get(url)
    handle(response)


######################## Weblab cache ########################
def cache(operation, data):
    if operation is None:
        operation = 'reset'

    if operation == 'reset':
        _reset_cache(data.get('namespace'))
    else:
        raise Exception('Operation %s is not supported' % operation)


def _reset_cache(namespace):
    if not namespace:
        raise Exception('namespace is empty')

    log('Resetting weblab states cache for namespace %s' % namespace)

    url = '{}/weblab-cache/{}/'.format(CRUD_ENDPOINT, namespace)
    response = requests.put(url)
    handle(response)


def run():
    try:
        result = parse_params()
    except Exception as ex:
        raise Exception('Passed args are invalid. {}'.format(ex.message))

    if not result['object']:
        raise Exception('Please run weblab.sh help to see command guide.')

    log('Parsed args: {}'.format(result))

    operation = result.get('operation')
    eval(result['object'])(operation, result)


if __name__ == '__main__':
    try:
        run()
    except Exception as ex:
        log('[ERROR] %s' % ex.message)