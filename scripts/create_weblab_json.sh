#!/bin/sh
# htwang@alauda.io
# 2018-5-30
echo '{' >/tmp/weblab.json
for i in /etc/config/*
do
    echo "    \"${i##*/}\": $(cat $i)," >>/tmp/weblab.json
done
sed -i '$s/,$//' /tmp/weblab.json
echo '}' >> /tmp/weblab.json
if cat /tmp/weblab.json  | python -m json.tool
then
    mv /tmp/weblab.json /weblab.json
else
    echo 'The weblab.json file is corrupted, not a json file'
fi
python /app/scripts/initialize_weblabs.py
