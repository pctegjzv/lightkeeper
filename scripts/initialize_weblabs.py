#!/usr/bin/env python
# yiyuan@alauda.io
# wanghongtao was revised in 2018-5-29

import sys
import requests
import os
import json

DEFAULT_WEBLAB_FILE = '/weblab.json'

endpoint = 'http://{}:8080/v1'.format(os.getenv('CRUD_ENDPOINT', '127.0.0.1'))


def create_weblab_with_state(name, is_enabled=False):

    template = {
        'name': name,
        'is_enabled': False,
        'description': name.upper(),
        'created_by': 'Genji auto create'
    }

    # Create weblab
    r = requests.post('{}/weblabs/'.format(endpoint), data = template)
    if 201 == r.status_code:

        # Update weblab
        update_url = '{}/weblabs/{}/'.format(endpoint, name)
        payload = {
            'is_enabled': is_enabled
        }
        update_response = requests.put(update_url, data=payload)

        if 200 == update_response.status_code:
            print 'Update weblab {} success.'.format(name)
            print update_response.status_code, update_response.content
        else:
            print 'Update weblab {} failed'.format(name)
            print update_response.content
    else:
       print 'create weblab {} failed'.format(name)
       print r.content

def remove_all():
    origin_weblab = get_current_weblabs()
    print "Delete old weblab"

    for weblab in origin_weblab:
        r = requests.delete('{}/weblabs/{}'.format(endpoint, weblab))
        if r.status_code != 204:
            print "Delete weblab failed"
            print r.content


def get_current_weblabs():
    r = requests.get('{}/weblabs'.format(endpoint))
    if r.status_code == 200:
        return map(lambda x: x['name'], r.json())
    else:
        print "Get weblab list failed."
        print r.content


if __name__ == '__main__':
    if os.path.exists(DEFAULT_WEBLAB_FILE):
        with open(DEFAULT_WEBLAB_FILE, 'r') as f:
            WEBLAB_LIST = json.load(f)
    else:
        print "Not find weblab.json files."
        sys.exit(1)

    remove_all()

    map(lambda x: create_weblab_with_state(x, WEBLAB_LIST[x]), WEBLAB_LIST)

